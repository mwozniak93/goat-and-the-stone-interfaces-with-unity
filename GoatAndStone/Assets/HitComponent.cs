﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitComponent : MonoBehaviour
{
	[SerializeField]
	float power;

	void OnTriggerEnter2D (Collider2D coll)
	{
		coll.gameObject.GetComponent<IDamagable> ().TakeDamage (power);
	}

}
