﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDamageComponent : MonoBehaviour, IDamagable
{
	public void TakeDamage (float amount)
	{
		Debug.Log ("Item taking " + amount + " damage");
		Explode ();
	}


	void Explode ()
	{
		Debug.Log ("Explode");
	
	}
}
