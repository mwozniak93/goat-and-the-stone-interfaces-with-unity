﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDamageComponent : MonoBehaviour, IDamagable
{
	public void TakeDamage (float amount)
	{
		Debug.Log ("Character taking " + amount + " damage");
		PushAway ();
	}

	void PushAway ()
	{
		Debug.Log ("PushAway");
	}

}
